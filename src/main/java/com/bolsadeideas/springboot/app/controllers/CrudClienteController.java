package com.bolsadeideas.springboot.app.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bolsadeideas.springboot.app.models.entity.Cliente;
import com.bolsadeideas.springboot.app.models.service.ICLiente;


@RestController
@RequestMapping("/Backend")
@CrossOrigin(origins = {"*"})
public class CrudClienteController {
	
	@Autowired
	private ICLiente clienteService;
	
	@GetMapping("/listar")
	public List<Cliente> index(){
		return clienteService.getCLientes();
	}

}
