package com.bolsadeideas.springboot.app.models.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.bolsadeideas.springboot.app.models.entity.EncabezadoEntrada;

public interface EncabezadoDao extends JpaRepository<EncabezadoEntrada, Long>{

}
