package com.bolsadeideas.springboot.app.models.entity;

import java.time.LocalDate;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Positive;

@Entity
@Table(name="encabezado_entrada")
public class EncabezadoEntrada {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idEncabezado;
	
	@Column(name="fecha_ingreso")
	private Date fechaIngreso;
	
	@Column(name="total_tiempo")
	private LocalDate totalTiempo;
	
	@Column(name="placa", length = 15, unique = true)
	private String nombre;
	
	@Positive
	@Column(name="cedula", length = 10)
    private Long cedula;
	
	@Column(name="id_tipo_vehiculo", length = 10)
    private Long idTipoVehiculo;
	
	@Column(name="id_turno", length = 10)
    private Long idTurno;
	
	@Column(name="hora_entrada")
	private LocalDate horaEntrada;
	
	@Column(name="hora_salida")
	private LocalDate horaSalida;
	
	@Column(name="valor_pagar")
	private Integer valorPagar;
	
	@Column(name="descuento")
	private Integer descuento;

	public EncabezadoEntrada() {
		
	}

	public Long getIdEncabezado() {
		return idEncabezado;
	}

	public void setIdEncabezado(Long idEncabezado) {
		this.idEncabezado = idEncabezado;
	}

	public Date getFechaIngreso() {
		return fechaIngreso;
	}

	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	public LocalDate getTotalTiempo() {
		return totalTiempo;
	}

	public void setTotalTiempo(LocalDate totalTiempo) {
		this.totalTiempo = totalTiempo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Long getCedula() {
		return cedula;
	}

	public void setCedula(Long cedula) {
		this.cedula = cedula;
	}

	public LocalDate getHoraEntrada() {
		return horaEntrada;
	}

	public void setHoraEntrada(LocalDate horaEntrada) {
		this.horaEntrada = horaEntrada;
	}

	public LocalDate getHoraSalida() {
		return horaSalida;
	}

	public void setHoraSalida(LocalDate horaSalida) {
		this.horaSalida = horaSalida;
	}

	public Integer getValorPagar() {
		return valorPagar;
	}

	public void setValorPagar(Integer valorPagar) {
		this.valorPagar = valorPagar;
	}

	public Integer getDescuento() {
		return descuento;
	}

	public void setDescuento(Integer descuento) {
		this.descuento = descuento;
	}

	@Override
	public String toString() {
		return "EncabezadoEntrada [idEncabezado=" + idEncabezado + ", fechaIngreso=" + fechaIngreso + ", totalTiempo="
				+ totalTiempo + ", nombre=" + nombre + ", cedula=" + cedula + ", horaEntrada=" + horaEntrada
				+ ", horaSalida=" + horaSalida + ", valorPagar=" + valorPagar + ", descuento=" + descuento + "]";
	}	
}
