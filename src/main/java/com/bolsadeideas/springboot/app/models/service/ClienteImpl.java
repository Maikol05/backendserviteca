package com.bolsadeideas.springboot.app.models.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.bolsadeideas.springboot.app.models.dao.ClienteDao;
import com.bolsadeideas.springboot.app.models.entity.Cliente;


@Service
public class ClienteImpl implements ICLiente{
	
	private ClienteDao clienteDao;

	@Transactional(readOnly = true)
	@Override
	public List<Cliente> getCLientes() {
		return (List<Cliente>)clienteDao.findAll();
	}

	@Override
	public Cliente saveCliente(Cliente cliente) {
		
		return null;
	}

	@Transactional(readOnly = true)
	@Override
	public Cliente findById(Long id) {
		
		return null;
	}

	@Override
	public void deleteById(Long id) {
				
	}

}
