package com.bolsadeideas.springboot.app.models.service;

import java.util.List;

import com.bolsadeideas.springboot.app.models.entity.Cliente;


public interface ICLiente {

	public List<Cliente> getCLientes();
	
	public Cliente saveCliente(Cliente cliente);
	
	public Cliente findById(Long id);
	
	public void deleteById(Long id);
}
