package com.bolsadeideas.springboot.app.models.service;

import java.util.List;
import com.bolsadeideas.springboot.app.models.entity.EncabezadoEntrada;

public interface IEncabezadoEntrada {
	
	public List<EncabezadoEntrada> getEncabezadoEntrada();
	
	public EncabezadoEntrada saveEncabezadoEntrada(EncabezadoEntrada EncabezadoEntrada);
	
	public EncabezadoEntrada findById(Long id);
	
	public void deleteById(Long id);

}
