package com.bolsadeideas.springboot.app.models.service;

import java.util.List;
import com.bolsadeideas.springboot.app.models.entity.Usuario;

public interface IUsuario {
	
public List<Usuario> getUsuarios();
	
	public Usuario saveUsuarios(Usuario usuario);
	
	public Usuario findById(Long id);
	
	public void deleteById(Long id);

}
